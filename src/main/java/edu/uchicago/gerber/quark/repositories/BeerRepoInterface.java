package edu.uchicago.gerber.quark.repositories;

import edu.uchicago.gerber.quark.models.Beer;

import java.util.List;

public interface BeerRepoInterface {

     List<Beer> getAll();
     List<Beer> add(Beer beer);
     Beer get( String id);
     List<Beer> paged(int num);
}
