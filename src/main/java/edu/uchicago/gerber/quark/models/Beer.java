package edu.uchicago.gerber.quark.models;

import com.github.javafaker.Faker;

public class Beer {


    private String id;
    private String hop;
    private String malt;
    private String yeast;
    private String name;
    private String style;

    public Beer(String id, String hop, String malt, String yeast, String name, String style) {
        this.id = id;
        this.hop = hop;
        this.malt = malt;
        this.yeast = yeast;
        this.name = name;
        this.style = style;
    }

    public Beer() { }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHop() {
        return hop;
    }

    public void setHop(String hop) {
        this.hop = hop;
    }

    public String getMalt() {
        return malt;
    }

    public void setMalt(String malt) {
        this.malt = malt;
    }

    public String getYeast() {
        return yeast;
    }

    public void setYeast(String yeast) {
        this.yeast = yeast;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }





}
