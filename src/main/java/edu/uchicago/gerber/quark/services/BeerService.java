package edu.uchicago.gerber.quark.services;

import edu.uchicago.gerber.quark.models.Beer;
import edu.uchicago.gerber.quark.repositories.BeerMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@ApplicationScoped
public class BeerService {

    @Inject
    BeerMongoRepository beerMongoRepository;


    public List<Beer> getAll() {
        return beerMongoRepository.getAll();
    }

    public List<Beer> add(Beer beer){
       return beerMongoRepository.add(beer);

    }

    public Beer get( String id) {
      return   beerMongoRepository.get(id);
    }


}
