package edu.uchicago.gerber.quark.resources;

import edu.uchicago.gerber.quark.models.Beer;
import edu.uchicago.gerber.quark.services.BeerService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/beers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BeerResource {

    @Inject
    BeerService beerService;

    @GET
    public List<Beer> getAll() {
        return beerService.getAll();
    }

    @POST
    public List<Beer> add(Beer beer){
       return beerService.add(beer);
    }

    @GET
    @Path("{id}")
    public Beer get(@PathParam("id") String id) {
        return beerService.get(id);
    }



}
